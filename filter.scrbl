#lang scribble/manual

@title{Filter}

Revisemos la función eliminar ya realizada en clase:

@codeblock{
;; positivos : List(Number) → List(Number)
(define (positivos ls)
        (match ls
        ;; En de tener la lista vacía devolvemos la lista vacía
           ['() '()]
        ;; Si tenemos la lista con al menos un elemento,
           [(cons x xs)
        ;; chequeamos si dicho elemento es mayor a 'w'
              (if (>= x 0)
                  ;; de ser así lo ponemos en el resultado, y continuamos con el
                  ;; resto de la lista.
                  (cons x (positivos xs))
                  ;; sino ignoramos, y continuamos con el resto de la lista.
                  (positivos xs)
              )]
        )
)
}

A diferencia de otras funciones, @code{positivos} como bien indica su nombre
puede llegar a modificar la lista que toma como argumento y basándonos en la
propiedad que vimos, @bold{no la podremos implementar usando @code{map}}. Sin
embargo en ella se oculta otro patrón muy utilizado.

Veamos como podemos hacer para abstraer de ella un patrón lo más general
posible. ¿Qué pasa si queremos eliminar los elementos distintos a @code{20}?¿Qué
pasa si queremos filtrar los elementos menores a @code{5}?. Podemos abstraer el
criterio por el cual filtramos, ¡lo logramos pidiéndolo como argumento!.

La función @code{filter} se encargará de @italic{filtrar/eliminar} ciertos
elementos de una lista. Toma como argumento una función @code{f : A → Boolean}
que es la que indica cuales son los elementos que van a ser filtrados y cuales
son los elementos que permanecerán en la lista resultante. Y como resultado
entonces tenemos una función de alto orden cuya definición es:
@codeblock{
;; filter : (A → Boolean) List(A) → List(A)
;; 
(define (filter f ls)
        ;; Dada una lista ls
        (match ls
               ;; En el caso de ser vacía, no hay nada que filtrar.
               ['() '()]
               ;; Si tiene al menos un elemento x
               [(cons x xs)
                  ;; Chequeamos si tenemos que insertar o ignorar a x utilizando f
                  ( if (f x)
                       ;; De ser aceptado lo agregamos a la lista resultante de
                       ;; filtrar con f
                       (cons x (filter f xs))
                       ;; De no ser aceptado simplemente lo ignoramos
                       (filter f xs)
                  )]
        )
)
}

Como aclaramos al principio, la función @code{filter} puede llegar a alterar la
lista. Pero, ¿qué otras propiedades se les ocurren? Notar que la función que hace
de filtro @bold{no modifica los elementos de la lista}.
