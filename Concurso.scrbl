#lang scribble/doc
@(require scribble/manual)


@title{Funciones de Alto Orden}
@author[(@author+email "Martín Ceresa" "martin@dcc.fceia.unr.edu.ar")]

Documento preparado para el Concurso 1492 para la obtención de un cargo de
Profesor Adjunto de dedicación Exclusiva. El documento fue escrito utilizando la
herramienta de documentación
Scribble[@url{https://docs.racket-lang.org/scribble}] y puede encontrarse en el
repositorio:@url{https://bitbucket.org/martinceresa/altoorden}.


Se identifica como la clase numero 11 dentro del plan de la asignatura
Programación I, dentro de la unidad 5 sobre listas.

Utilizaremos particularmente:
@itemlist[
 @item{Listas: constructores}
 @item{Recursión}
 @item{@code{match} como mecanismo de pattern matching}
]

La clase sobre Funciones de Alto Orden se encuentre luego de realizar al menos
un encuentro de práctica sobre listas.

@section{Introducción a Funciones de Alto Orden}

Llamaremos funciones de alto orden a @bold{toda función que tome como argumento
una función y/o devuelva como resultado una función}.

Veamos un ejemplo:
@codeblock{
  ;; aplicar : (A → B) A → B
  ;; aplicar toma una función (f : A → B),
  ;; un elemento (x : A) y nos devuelve
  ;; el resultado de aplicar f en x, (f x : B).
  (define (aplicar f x) (f x))
}

La función @code{aplicar} toma como argumento un elemento de tipo @bold{función}, y
por esto decimos que @code{aplicar} @bold{es una función de alto orden}.

Veamos otro ejemplo un poco más complicado:

@codeblock{
  ;; componer : (A → B) (B → C) A → C
  ;; componer toma dos funciones (f : A → B),
  ;; (g : B → C) y un elemento de (x : A),
  ;; y nos devuelve la composición de f con g, g(f(x)).
  (define (componer f g x)
          (aplicar g (aplicar f x)))
	;; == (g (f x))
}

Así entonces podemos decir que @code{componer} es una función de alto orden, ya
que toma como argumento @italic{dos} funciones.

Al principio de la sección definimos a las funciones de alto orden como aquellas
que tomaban como argumento funciones y también que devolvían funciones como
resultado. @bold{¿Cómo hacemos entonces para devolver una función?}.  Para esto
se introducirá una nueva forma de construir funciones, que lo veremos en la
práctica, pero como adelanto podremos utilizar las denominadas @italic{funciones
anónimas}.

@section{Funciones de Alto Orden con Listas}

La semana pasada vimos algunas funciones sobre listas y ¡realizamos un montón de ejercicios!
En esta clase continuaremos estudiando funciones sobre listas, aunque esta vez el objetivo
será encontrar patrones comunes, y utilizar dichos patrones para la construcción
modular de programas complejos.

Vamos a definir 3 funciones de alto orden:
@itemlist[
  @item{@code{map}: que nos permite aplicar una función a los elementos de una
lista.}
  @item{@code{filter}: que nos permite filtrar ciertos elementos de una lista.}
  @item{@code{fold}: que nos permite consumir, o utilizar tanto los elementos de
la lista como la estructura de la lista, para obtener valores de otro tipo.}
]

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@include-section["map.scrbl"]
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@include-section["filter.scrbl"]
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@include-section["fold.scrbl"]
