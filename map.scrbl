#lang scribble/manual

@title{Map}

Veamos un ejemplo que utilizamos la clase pasada:

@codeblock{
;; cuadrado : Number → Number
;; cuadrado toma un número lo eleva al cuadrado.
(define (cuadrado n) (* n n))

;; cuadrados : List(Number) → List(Number)
;; cuadrados toma una lista nos devuelve la lista
;; aplicando cuadrado a cada uno de sus elementos.
(define (cuadrados ls)
        (match ls
           ['() '()] ;; Lista vacía devuelvo lista vacía
           [(cons x xs) ;; Si la lista tiene al menos un elemento
             (cons
                (cuadrado x) ;; elevamos al cuadrado de ese elemento
                (cuadrados ls) ;; y elevamos al cuadrado al resto de la lista
             )]
        )
)
}

La función @code{cuadrados} aplica la función @code{cuadrado} a cada elemento de
la lista que toma como argumento. Logra su objetivo recorriendo la lista que
toma como argumento @italic{matcheando} su constructores. 

Concretamente obtenemos:
@codeblock{
(cuadrados (list a1 a2 ... an))
==
(cuadrados (cons a1 (list a2 ... an)))
==
(cons (cuadrado a1) (cuadrados (list a2 ... an)))
==
(list (cuadrado a1) (cuadrado a2) ... (cuadrado an))
}

El resultado obtenido es el de aplicar la función @code{cuadrado}
a cada elemento de la lista @code{(list a1 a2 ... an)}.
Como ya hemos visto en la práctica anterior, éste patrón es muy utilizado. 

La idea fundamental de éste tipo de funciones es recorrer la lista aplicando
una función a cada uno de los elementos de ella. Cómo @bold{no sabemos} qué
función vamos a utilizar, la pedimos como argumento, obteniendo así una función
de alto orden.

Cuya definición está dada por
@codeblock{
;; map : (A → B) List(A) → List(B)
;; map f '() = '()
;; map f (list a1 a2 ... an) = (list (f a1) (f a2) ... (f an))
(define (map f ls)
        (match ls
               ['() '()]
               [(cons x xs) (cons (f x) (map f xs))]
        )
)
}

Finalmente podemos definir funciones como @code{cuadrados} simplemente como:
@codeblock{
(define (cuadrados l) (map cuadrado l))
}

La función @code{map} es muy útil ya que posee propiedades interesantes, aunque
en su mayoría van por fuera del alcance del curso. Sin embargo, una propiedad
simple de ver es que @code{map} @bold{no} cambia la longitud
de la lista, e incluso podríamos decir que @italic{estructuralmente} la
@bold{lista no cambió}, simplemente cambió la información que esta traía.