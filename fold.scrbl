#lang scribble/manual

@title{Fold}

Hasta ahora hemos visto funciones que siempre devuelven una lista, que pasa si
queremos @italic{consumir} la información de una lista para obtener un nuevo
tipo de datos.

Por ejemplo, como ya hemos visto, podemos sumar todos los elementos de una
lista de números de la siguiente forma:
@codeblock{
;; sumas :: Lista(Number) → Number
;; sumas nos devuelve la suma de todos los números dentro de una lista.
(define (sumas ls)
        (match ls
               ['() 0]
               [(cons x xs) (+ x (sumas xs))])
)
}

Si expandemos dicha función obtendremos algo así:
@codeblock{
(sumas (list 1 2 35))
==
(sumas (cons 1 (cons 2 (cons 35 '()))))
==
(+ 1 (sumas (cons 2 (cons 35 '()))))
==
(+ 1 (+ 2 (sumas (cons 35 '()))))
==
(+ 1 (+ 2 (+ 35 (sumas '()))))
==
(+ 1 (+ 2 (+ 35 0)))
}

Es decir, la función @code{sumas} va reemplazando cada aparición
del constructor @code{cons} por la suma entre en la cabeza de la lista
y el resultado de sumar el resto de la lista. Hasta alcanzar la lista
vacía que retorna @code{0}.

De nuevo, éste es un patrón muy común al momento de hacer funciones que
consuman los datos de una lista! Notar que una lista está construida usando
@code{cons} de elementos a partir de una lista vacía @code{'()}, entonces
podemos pensar que una función que decida qué hacer para cada uno de estos
casos esconde un patrón muy general.

Comencemos por abstraer los valores concretos utilizados en la función @code{sumas}:
@itemlist[
  @item{en vez de números concretos vamos a operar con elementos @code{a1 : A,
a2 : A, ... , an : A},}
  @item{un valor @code{c : B}, que será el valor a reemplazar por la lista
vacía,}
  @item{y una función @code{f : A B → B}.}
]

Vayamos ahora al reves de @code{sumas}. Generalizando:
@itemlist[
  @item{la operación de sumar @code{+} por la función @code{f}}
  @item{el valor @code{0} por el valor @code{c}}
  @item{la lista @code{(list 1 2 35)} por @code{(list a1 a2 ... an)}}
]

Obteniendo entonces:
@codeblock{
f a1 (f a2 (...(f an c)...))
==
fold f c (cons a1 (cons a2 (... (cons an '())...)))
==
fold f c (list a1 a2 ... an)
}

Y lo implementamos de la siguiente forma

@codeblock{
;; fold : (A B → B) B List(A) → B
(define (fold f c l)
        (match l
        ['() c]
        [(cons x xs) (f x (fold f c xs))]
        )
)
}

Lo que hace @code{fold} es reemplazar cada @code{cons} por una aplicación de
@code{f} tomando el primer argumento a cabeza de la lista y el resultado
de la llamada recursiva sobre el resto de la lista utilizando
@code{f} y @code{c}.

Podemos entonces implementar la función @code{sumas} como:
@codeblock{
(define (sumas l) (fold suma 0 l))
}

La función @code{fold} es un más general que @code{filter} y @code{map}, ya que
podemos implementarlas en usando @code{fold}.

Ejercicios:
@codeblock{
(define (map f l) (fold ? ? l))
(define (filter f l) (fold ? ? l))
}

Finalmente, la forma en que definimos @code{fold} es también conocida como
@code{foldr} por la forma en que se asocian (a derecha) las aplicaciones de la
función que toma como argumento. Existe, por analogía, una función @code{foldl}
que asocia la aplicación de la función que toma como argumento @italic{a
izquierda}. Aunque en este curso consideraremos solamente @code{foldr} y
basándonos en su generalidad la llamaremos simplemente @code{fold}.
